<?php

// +----------------------------------------------------------------------
// | 谱创科技
// +----------------------------------------------------------------------
// | 版权所有 2018~2022 吉林省谱创科技发展有限公司 [ http://www.7dcs.com ]
// +----------------------------------------------------------------------
// | Author: 宝龙  QQ: 117384790  Wechat: bao807735
// +----------------------------------------------------------------------

namespace baolong;

use app\wechat\service\WechatService;
use baolong\message\Contracts\DataArray;
use baolong\message\model\BaseTemplateMessageConfig;
use baolong\message\model\BaseTemplateMessageFlow;
use baolong\message\service\SmsService;
use think\admin\Exception;
use think\admin\Service;
use WeChat\Exceptions\InvalidResponseException;
use WeChat\Template;
use WeMini\Newtmpl;

/**
 * Class unionPay
 * @package unionPay
 */
class TemplateMessage extends Service {
    /**
     * 支持配置
     * @var DataArray
     */
    protected $config;
    /**
     * 消息模板项目
     * @var DataArray
     */
    protected $project;
    /**
     * 定义当前版本
     * @var string
     */
    const VERSION = '1.0.2';

    public function __construct(){
        $this->config = new DataArray(app()->config->get('template_message'));
        $this->config->set('wemini',sysdata('wxapp'));
        $this->config->set('wechat',sysconf('wechat.'));
    }

    /**
     * 获取消息模板项目
     * @return DataArray
     */
    public function getProject(){
        $project = $this->config->get('project');
        return BaseTemplateMessageConfig::mk()->getList($project);
    }

    /**
     * 获取但项目
     * @return void
     */
    public function getProjectInfo($key='',$code=''){
        [$project,$data] = [$this->getProject(),[]];
        foreach ($project as $proKey => $pro) foreach ($pro['list'] as $listKey => $list) foreach ($list['data'] as $info)  {
            [$info['base']['title'],$info['base']['key']] = [$list['title']??'',$listKey??''];
            $data[$list['key']][$info['code']] = $info;
        }
        if (empty($key) && empty($code)) return $data??[];
        if (!empty($key) && empty($code)) return $data[$key]??[];
        if (!empty($key) && !empty($code)) return $data[$key][$code]??[];
    }

    /**
     * 初始化模板数据
     * @return void
     */
    public function initTemplate(array $data = []){
        if ( empty($data) ) return false;
        if (!in_array($data['code'],['wemini','wechat']))  throw new Exception('CODE参数错误');

        // 获取微信配置参数
        $config = $this->config->get($data['code'])??[];
        // 获取模块项目配置参数
        $projectInfo = TemplateMessage::instance()->getProjectInfo($data['key'],$data['code']);
        // 获取已存在的数据
        $messageInfo = BaseTemplateMessageConfig::mk()->where(['key'=>$data['key'],'code'=>$data['code']])->findOrEmpty();

        if ($data['code']=='wemini'){
            $Newtmpl = Newtmpl::instance($config);
            $kid_list = $projectInfo['kid_list']??[];
            if ( empty($kid_list) ) throw new Exception('配置项 kid_list 不能为空！');
            if (empty($projectInfo['short_id']) )  throw new Exception('未定义模板ID');
            try {
                // 如果存在数据，并且有模板则进行删除模板操作
                if (!$messageInfo->isEmpty() && isset($messageInfo['template_id']))  $Newtmpl->delTemplate($messageInfo['template_id']);
                // 进行创建 并获取模板列表
                if ( ( $tempInfo = $Newtmpl->addTemplate($projectInfo['short_id'],$kid_list,$projectInfo['sceneDesc']??'珍实惠三维一体门店') ) && ( $templist = $Newtmpl->getTemplateList() )   ) {
                    // 获取 模板内容
                    $info = array_values(array_filter($templist['data'],function ($res) use ($tempInfo){
                            return $res['priTmplId'] == $tempInfo['priTmplId'] ;
                        }))[0]??[];
                    if ($info){
                        // 写入模板数据
                        BaseTemplateMessageConfig::mk()->insertGetId([
                            'key' => $data['key'],'code' => $data['code'],'tmp_title' => $info['title'],
                            'kid_list' => arr2str($kid_list),'template_id' => $info['priTmplId'],
//                            'relation_template' => arr2str([$info['priTmplId']]),
                            'short_id' => $projectInfo['short_id'],'content' => $info['content'],'example' => $info['example'],
                        ]);
                    }else{
                        $Newtmpl->delTemplate($tempInfo['priTmplId']);
                    }
                }
                return true;
            } catch (Exception|InvalidResponseException $exception){
                throw new Exception($exception->getMessage());
            }
        }else if ($data['code']=='wechat'){
            throw new Exception('对接开发中..');
        }
    }

//    public function post($key){
//        $config = $this->config->get('wemini')??[];
//        $Newtmpl = Newtmpl::instance($config);
//       return  $Newtmpl->send([
//            'touser' => 'oVKzD5ERnB96xB935cUDuQ182Rm4',
//            'template_id' => 'fX9QHCclrwBD3Vk97NOvqEJCV_x5E_pTXLHDc4GlSZc',
//            'page' => '/packageA/product/detail?code=G7582229140747092243',
//            'miniprogram_state' => 'developer',  //developer为开发版；trial为体验版；formal为正式版；默认为正式版
//            'lang' => 'zh_CN',
//            'data' => [
//                'thing1' => ['value' => '111111'],
//                'amount4' => ['value' => '2222'],
//                'thing7' => ['value' => '333'],
//                'thing8' => ['value' => '4444'],
//            ],
//        ]);
//    }

    /**
     * 发送短信
     * @param string $mobile 接收的手机号
     * @param array $data 参数  ['key'=>'项目KEY','openid'=>'会员openid']
     * @return array|void
     */
    public function mobileSend(string $mobile,array $data){
        [$openid,$key,$code] = [$data['openid']??'',$data['key']??'','sms'];
        $info = BaseTemplateMessageConfig::mk()->where(['key' => $key,'status' => 1,'code' => 'sms'])->findOrEmpty();
        if ($info->isEmpty()) return [0, '模板未定义', []];
        // 验证码类型短信 即时发送内容
        if (substr($key,0, 5) == 'SIGIN') {
            if (empty($mobile)) return [0, '手机号码不能为空', []];
            if (empty($info['content'])) return [0, '未定义短信内容', []];
            return SmsService::instance()->sendCode($mobile, $info['content'] ,['openid' => $openid, 'key'=> $key, 'code' => $code]); // 验证码类型
        }else{
            return [0, '项目类型错误', []];
        }
    }

    /**
     * 验证手机短信验证码
     * @param string $code
     * @param string $phone
     * @param string $key
     * @return bool
     */
    public function mobileCheckCode(string $code, string $phone, string $key = ''){
       return SmsService::instance()->checkCode($code,$phone,$key);
    }

    /**
     * 清楚短信验证码缓存
     * @param string $phone
     * @param string $key
     * @return bool
     */
    public function mobileRemoveCode(string $phone, string $key = ''){
        return SmsService::instance()->removeCode($phone,$key);
    }



    /**
     * 创建预约记录
     * @return void
     */
    public function make($key,$data=[]){
        $congig = BaseTemplateMessageConfig::mk()->where(['key' => $key,'status' => 1]);
        foreach ($congig->cursor() as $item) try {
            if ($item['code'] == 'sms'){ //短信  短信即时发送，不会创建任务
                if (empty($data['mobile'])) return [0, '手机号码不能为空', []];
                if (empty($item['content'])) return [0, '未定义短信内容', []];
            }
            // 创建预约数据
            BaseTemplateMessageFlow::mk()->insert([
                'key' => $item['key'], 'code' => $item['code'], 'content' => static::fieldFilter($item,$data),  'status' => 1,
            ]);
        } catch (Exception $exception){

        }
    }



    // 根据响应的模板内容进行替换
    private function fieldFilter($config,$data=[]){
        $content = $config['content'];
        // 开始内容替换吧 哈哈
        
        return $content;
    }

}