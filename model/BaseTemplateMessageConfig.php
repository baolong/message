<?php

// +----------------------------------------------------------------------
// | ThinkAdmin
// +----------------------------------------------------------------------
// | 版权所有 2014~2022 广州楚才信息科技有限公司 [ http://www.cuci.cc ]
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/ThinkLibrary
// | github 代码仓库：https://github.com/zoujingli/ThinkLibrary
// +----------------------------------------------------------------------

declare (strict_types=1);

namespace baolong\message\model;

use think\admin\Model;

/**
 * 用户权限模型
 * Class SystemAuth
 * @package think\admin\model
 */
class BaseTemplateMessageConfig extends Model
{
    /**
     * @param array $project
     * @param array $configData
     * @return array
     */
    public function getList(array $project,array $configData = []){
        $db =  static::mk();
        foreach ($db->cursor() as $vo) {
            $info = $vo->toArray();
            $info['relation_template_data'] = array_merge([$info['template_id']],str2arr($info['relation_template'])??[]);
            if (in_array($info['code'],['wemini','wechat'])){

            }
            $configData[$vo['key']][$vo['code']] = $info;
        }
        foreach ($project as &$pro) foreach ($pro['list'] as &$list) foreach ($list['data'] as &$data)   $data['config'] = $configData[$list['key']][$data['code']]??[];
        return $project;
    }
     
}